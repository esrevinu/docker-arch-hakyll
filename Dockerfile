FROM archlinux:latest as build

RUN sed -i -e "/^\[core\]/i [testing]\nInclude = /etc/pacman.d/mirrorlist\n" -e "/^\[community\]/i [community-testing]\nInclude = /etc/pacman.d/mirrorlist\n" /etc/pacman.conf

RUN pacman -Syu --noconfirm
RUN pacman -S --noconfirm ghc cabal-install \
           haskell-hakyll haskell-lens make
RUN yes | pacman -Scc
RUN rm -rf /var/lib/pacman/sync/*

ADD ./aws-s3-sync  /usr/bin/aws-s3-sync
ADD ./cabal_config /root/.cabal/config
RUN cabal update
RUN cabal install --ghc-option=-dynamic --lib hakyll-sass
RUN rm -rf /root/.cabal/packages/hackage.haskell.org/01-index.tar.gz
RUN find /root/.cabal/packages/hackage.haskell.org/ -type d -maxdepth 1 -mindepth 1 -exec rm -rf {} \;

FROM scratch
COPY --from=build / /
ENV LANG=en_US.UTF-8
CMD ["/usr/bin/bash"]
